## Cách tạo project với NextJS

- `npx create-next-app`
- nếu bạn chưa từng cài react vào máy của mình thì `npm install create-react-app`, sau đó nhập `npm create-next-app`, rồi mới đến nhập tên project mà bạn muốn
- sau đó nhập vào tên của project

## Cách viết mã TS trong ứng dụng NextJs

- B1 : Tạo 1 file ngoài thư mục root tên : tsconfig.json (viết chính xác tsconfig.json)
- B2 : `yarn --dev typescript @types/react @types/node` hoặc `npm install --save-dev typescript @types/react @types/node`

## Chạy ứng dụng

- Để chạy ứng dụng tại terminal : `yarn dev` hoặc `npm run dev`
