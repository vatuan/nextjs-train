// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { NextApiRequest, NextApiResponse } from 'next'   // ==> đây là 2 kiểu dữ liệu do Next tự định nghĩa
export default (req: NextApiRequest, res: NextApiResponse) => {
  res.statusCode = 200;
  res.json({ name: "Vũ Anh Tuấn" });
};
